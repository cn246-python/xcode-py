## xcode-py

Here's my second attempt at converting flac files to mp3, only this time I've written it in Python.

I re-coded this script as an opportunity to learn Python programming fundamentals. I'm sure there are plenty of things that can be improved.

----

### Required Packages
I wrote this using Debian 10.3 (Buster).

The external programs are doing the processing because I couldn't find any definitive guides on tools that are available exclusively in Python. Most guides I've read recommend using `lame` and/or `sox` to do the processing properly.

- `python 3.7.3`
- `flac`
- `lame`
- `sox`

----

### How to use
There is a variable in the file titled `outdir`. If you use the `--newdir` flag, this is where the resulting .mp3 files will go, along with the relevant files for the album. 

I prefer to link my scripts to `$PATH` so I can run them from any directory.

1. `ln -s /path/to/xcode-py /path/to/$PATH`
2. `cd flac/album/dir`
3. `xcode-py --help` Run help to see available options
4. `xcode-py` 

----

#### Other 
I know this works in Linux. I doubt it works in Windows. Not sure about Mac.

